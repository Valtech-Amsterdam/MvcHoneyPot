﻿using System.Web.Mvc;
using Valtech.Amsterdam.Labs.MvcHoneyPot.Filters;

namespace Valtech.Amsterdam.Labs.MvcHoneyPot
{
    public class MvcHoneyPotConfig
    {
        /// <summary>
        /// Register the actionfilter for the honeypot validation
        /// </summary>
        /// <param name="filters"></param>
        public static void Register(GlobalFilterCollection filters)
        {
            filters.Add(new MvcHoneyPotActionFilterAttribute());
        }
    }
}
