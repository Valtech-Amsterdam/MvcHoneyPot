﻿using System;
using System.Linq;
using System.Web.Mvc;
using Valtech.Amsterdam.Labs.MvcHoneyPot.Services;

namespace Valtech.Amsterdam.Labs.MvcHoneyPot.Filters
{
    public class MvcHoneyPotActionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ValidateHoneyPot(filterContext);
            base.OnActionExecuting(filterContext);
        }

        private void ValidateHoneyPot(ActionExecutingContext filterContext)
        {
            var honeyPotService = DependencyResolver.Current.GetService<IMvcHoneyPotService>();
            var requestParameters = filterContext.RequestContext.HttpContext.Request.Params;
            if (honeyPotService.SelectedInputIdentifier == null) return;

            // Find a request parameterkey that matches the SelectedInputIdentifier
            var requestParameterKey = requestParameters.AllKeys.SingleOrDefault(
                key => key.Equals(honeyPotService.SelectedInputIdentifier,
                        StringComparison.InvariantCultureIgnoreCase));
            if (String.IsNullOrWhiteSpace(requestParameterKey)) return;
            // Get the request value corresponding to the honeypot
            var requestParameterValue = requestParameters[requestParameterKey];
            if (String.IsNullOrWhiteSpace(requestParameterValue)) return;

            // Oh noes! You iz bot
            filterContext.Controller.ViewData.ModelState
                .AddModelError(honeyPotService.Settings.ErrorKey, honeyPotService.GetErrorMessage());
        }
    }
}