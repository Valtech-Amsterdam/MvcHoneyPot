﻿using System;
using System.Linq;
using System.Web;

namespace Valtech.Amsterdam.Labs.MvcHoneyPot.Services
{
    /// <summary>
    /// A default implementation of the MvcHoneyPotService
    /// </summary>
    public class DefaultMvcHoneyPotService : IMvcHoneyPotService
    {
        public static readonly string DefaultMvcHoneyPotServiceSessionInputIdentiefier =
            nameof(DefaultMvcHoneyPotServiceSessionInputIdentiefier);
        protected readonly Random _randomizer;

        /// <summary>
        /// A default implementation of the MvcHoneyPotService
        /// </summary>
        public DefaultMvcHoneyPotService()
        {
            _randomizer = new Random();
        }

        public virtual MvcHoneyPotSettings Settings => new MvcHoneyPotSettings();

        public virtual string SelectedInputIdentifier => HttpContext.Current?.Session?[DefaultMvcHoneyPotServiceSessionInputIdentiefier]?.ToString();

        public virtual string GetRandomInputIdentifier()
        {
            if (HttpContext.Current?.Session == null) return "NoSession";

            var randomizedIdentifiers = Settings.InputIdentifiers.OrderBy(x => _randomizer.Next());
            var left = randomizedIdentifiers.First();
            var right = randomizedIdentifiers.Skip(1).First();
            var inputIdentifier = $"{left}-{right}";

            HttpContext.Current.Session[DefaultMvcHoneyPotServiceSessionInputIdentiefier] = inputIdentifier;

            return inputIdentifier;
        }

        public virtual string GetErrorMessage()
        {
            return "You might be a bot!";
        }
    }
}
