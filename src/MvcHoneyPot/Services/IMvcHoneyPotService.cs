﻿namespace Valtech.Amsterdam.Labs.MvcHoneyPot.Services
{
    /// <summary>
    /// The service that is responsible for managing the honeypots
    /// </summary>
    public interface IMvcHoneyPotService
    {
        /// <summary>
        /// The settings for the MvcHoneyPot
        /// </summary>
        MvcHoneyPotSettings Settings { get; }
        /// <summary>
        /// The input identifier currently selected in the session
        /// </summary>
        string SelectedInputIdentifier { get; }
        /// <summary>
        /// Get a random identifier and set it in the session see <see cref="SelectedInputIdentifier"/> to retreive this value.
        /// </summary>
        /// <returns></returns>
        string GetRandomInputIdentifier();
        /// <summary>
        /// A method for adding an error message to the modelstate
        /// </summary>
        /// <returns></returns>
        string GetErrorMessage();
    }
}