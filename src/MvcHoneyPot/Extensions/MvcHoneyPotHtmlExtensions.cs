﻿using System;
using System.Web;
using System.Web.Mvc;
using Valtech.Amsterdam.Labs.MvcHoneyPot.Services;

namespace Valtech.Amsterdam.Labs.MvcHoneyPot.Extensions
{
    public static class MvcHoneyPotHtmlExtensions
    {
        /// <summary>
        /// Generate a honeypot input and register the identifier automatically
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <returns></returns>
        public static IHtmlString HoneyPot(this HtmlHelper htmlHelper)
        {
            var honeyPotService = DependencyResolver.Current.GetService<IMvcHoneyPotService>();
            var builder = new TagBuilder("input");
            if(!String.IsNullOrEmpty(honeyPotService.Settings.InputClassName))
                builder.Attributes.Add("class", honeyPotService.Settings.InputClassName);
            
            var inputId = honeyPotService.GetRandomInputIdentifier(); // <-- This registers automatically
            builder.Attributes.Add("name", inputId);
            builder.Attributes.Add("id", inputId);
            if(honeyPotService.Settings.DisableAutoComplete)
                builder.Attributes.Add("autocomplete", "off"); // <-- I hope this doesn't help with detecting honeypots

            return new HtmlString(builder.ToString());
        }
    }
}
