﻿using System.Web.Mvc;
using Valtech.Amsterdam.Labs.MvcHoneyPot.Example.Models;

namespace Valtech.Amsterdam.Labs.MvcHoneyPot.Example.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(new ExampleFormViewModel());
        }

        [HttpPost, ValidateAntiForgeryToken] // <-- So you don't need an extra attribute for every form
        public ActionResult Index(ExampleFormViewModel model)
        {
            return View(model);
        }
    }
}