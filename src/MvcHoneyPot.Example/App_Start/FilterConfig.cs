﻿using System.Web.Mvc;

namespace Valtech.Amsterdam.Labs.MvcHoneyPot.Example
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
