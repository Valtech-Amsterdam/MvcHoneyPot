﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using Valtech.Amsterdam.Labs.MvcHoneyPot.Example.Services.HoneyPot;
using Valtech.Amsterdam.Labs.MvcHoneyPot.Example.Services.Localization;
using Valtech.Amsterdam.Labs.MvcHoneyPot.Services;

namespace Valtech.Amsterdam.Labs.MvcHoneyPot.Example
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            MvcHoneyPotConfig.Register(GlobalFilters.Filters);

            ConfigureContainer();
        }

        /// <summary>
        /// Example of registering the <see cref="IMvcHoneyPotService"/>
        /// </summary>
        /// <param name="builder"></param>
        private static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<FakeLocalizationService>().As<ILocalizationService>();
            builder.RegisterType<CustomMvcHoneyPotService>().As<IMvcHoneyPotService>();
        }

        #region IOC Container
        /// <summary>
        /// Register the IOC Container and initialize services.
        /// I realize this would normaly be done in different files, this is just an example tho
        /// </summary>
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            // Register dependencies in controllers
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // Register dependencies in filter attributes
            builder.RegisterFilterProvider();

            // Register dependencies in custom views
            builder.RegisterSource(new ViewRegistrationSource());

            RegisterTypes(builder);

            // Set MVC DI resolver to use our Autofac container
            DependencyResolver.SetResolver(new AutofacDependencyResolver(builder.Build()));
        }
        #endregion
    }
}