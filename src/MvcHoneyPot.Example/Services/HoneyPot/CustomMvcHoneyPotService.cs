﻿using Valtech.Amsterdam.Labs.MvcHoneyPot.Example.Services.Localization;
using Valtech.Amsterdam.Labs.MvcHoneyPot.Services;

namespace Valtech.Amsterdam.Labs.MvcHoneyPot.Example.Services.HoneyPot
{
    /// <summary>
    /// An example of how to override the <see cref="DefaultMvcHoneyPotService"/>
    /// with some custom properties and functionalities
    /// </summary>
    public sealed class CustomMvcHoneyPotService : DefaultMvcHoneyPotService
    {
        private readonly ILocalizationService _localizationService;
        /// <summary>
        /// An example of how to override the <see cref="DefaultMvcHoneyPotService"/>
        /// with some custom properties and functionalities
        /// </summary>
        public CustomMvcHoneyPotService(ILocalizationService localizationService)
        {
            // Add custom words to the input identifiers
            Settings.InputIdentifiers.Add("Noot");

            _localizationService = localizationService;
        }

        // Call localization service for an actual error message
        public override string GetErrorMessage() =>
            _localizationService.GetText("/Validation/HoneyPotError");

        public override MvcHoneyPotSettings Settings => new MvcHoneyPotSettings
        {
            InputClassName = "emphasize-example" // <-- Set to hidden or another class that doesn't display the input
        };
    }
}