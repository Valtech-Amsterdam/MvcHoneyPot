﻿namespace Valtech.Amsterdam.Labs.MvcHoneyPot.Example.Services.Localization
{
    public interface ILocalizationService
    {
        string GetText(string textEntryPath);
    }
}