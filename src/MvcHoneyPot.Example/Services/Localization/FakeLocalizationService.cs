﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Valtech.Amsterdam.Labs.MvcHoneyPot.Example.Services.Localization
{
    public class FakeLocalizationService : ILocalizationService
    {
        public string GetText(string textEntryPath) =>
            $"Localized text for {textEntryPath} " +
            $"in current culture [{HttpContext.Current.Request.UserLanguages.First()}]";
    }
}