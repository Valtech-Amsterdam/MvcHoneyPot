﻿using System.ComponentModel.DataAnnotations;

namespace Valtech.Amsterdam.Labs.MvcHoneyPot.Example.Models
{
    public class ExampleFormViewModel
    {
        [Required]
        public string SomeRequiredField { get; set; }
        public string SomeOtherField { get; set; }
    }
}