# Changelog

## Release 1.0.3 (latest)
* Fixed autobuild
* Removed NuGet injections
* Added Contribution guide
* Added Changelog

## Release 1.0.2
* Update nuspec descriptions
* Made disabling autocomplete optional

## Release 1.0.1
* Added a custom logo
* Cleaned up the solution a bit
* Added some styling to the example project

## Release 1.0.0 Initial Release
* Initial publication